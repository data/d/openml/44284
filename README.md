# OpenML dataset: Meta_Album_SPT_Mini

https://www.openml.org/d/44284

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album Sports Actions Dataset (Mini)**
***
The 100-Sports dataset(https://www.kaggle.com/datasets/gpiosenka/sports-classification) is a collection of sports images covering 73 different sports. Images are 224x224x3 in size and in .jpg format. Images were gathered from internet searches. The images were scanned with a duplicate image detector program and all duplicate images were removed. For Meta-Album, the dataset is preprocessed and images are resized into 128x128 pixels using Open-CV.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/SPT.png)

**Meta Album ID**: HUM_ACT.SPT  
**Meta Album URL**: [https://meta-album.github.io/datasets/SPT.html](https://meta-album.github.io/datasets/SPT.html)  
**Domain ID**: HUM_ACT  
**Domain Name**: Human Actions  
**Dataset ID**: SPT  
**Dataset Name**: Sports Actions  
**Short Description**: 100 Sports Dataset  
**\# Classes**: 73  
**\# Images**: 2920  
**Keywords**: human actions, sports  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: CC0 1.0 Public Domain  
**License URL(original data release)**: https://www.kaggle.com/gpiosenka/sports-classification
https://creativecommons.org/publicdomain/zero/1.0/
 
**License (Meta-Album data release)**: CC0 1.0 Public Domain  
**License URL (Meta-Album data release)**: [https://creativecommons.org/publicdomain/zero/1.0/](https://creativecommons.org/publicdomain/zero/1.0/)  

**Source**: 100 Sports Image Classification  
**Source URL**: https://www.kaggle.com/gpiosenka/sports-classification  
  
**Original Author**: Gerald Piosenka  
**Original contact**: https://www.kaggle.com/gpiosenka/contact  

**Meta Album author**: Jilin He  
**Created Date**: 01 March 2022  
**Contact Name**: Ihsan Ullah  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@article{100-sports,
    title={100 Sports Image Classification},
    author={Gerald Piosenka},
    url={https://www.kaggle.com/datasets/gpiosenka/sports-classification},
    publisher= {Kaggle}
}
```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Micro]](https://www.openml.org/d/44240)  [[Extended]](https://www.openml.org/d/44319)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44284) of an [OpenML dataset](https://www.openml.org/d/44284). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44284/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44284/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44284/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

